---
title: "R Notebook"
output: html_notebook
---


```{r}
library(ggplot2)
library(dplyr)
library(tidyr)
library(tibbletime)
library(openxlsx)
library(timevis)
library(vistime)
library(stringr)

plotEditorsTenure <- function() {
  authors <- read.xlsx("authors-reconciled.xlsx") |>
    mutate(
      Geburtsdatum <- ifelse(is.na(Geburtsdatum), "", substr(Geburtsdatum, 1,4)),
      Sterbedatum <- ifelse(is.na(Sterbedatum), "", substr(Sterbedatum, 1,4))
    )
  editors <- read.xlsx("editors.xlsx")  |>
    merge(authors, by="Name") |>
    mutate(
      content <- paste(Name," (", Geburtsdatum, "-", Sterbedatum, ")", sep=""),
      title <- content,
      start <- paste(start, "-06-01", sep=""),
      end <- str_c(ifelse(is.na(end), "2030", end), "-06-01"),
      #event <- content
    )
  timevis(editors)
}

```

Now, click the **Run** button on the chunk toolbar to [execute](https://www.jetbrains.com/help/pycharm/r-markdown.html#run-r-code) the chunk code. The result should be placed under the chunk.
Click the **Knit and Open Document** to build and preview an output.
