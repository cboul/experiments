---
title: "ZfRsoz gender of authors"
output: html_notebook
---


```{r}
library(ggplot2)
library(dplyr)
library(tidyr)
library(tibbletime)
library(openxlsx)
library(timevis)
library(vistime)
library(stringr)

# Gender of authors
plotAuthorsGender = function(authors) {
  barplot(table(authors$gender), ylab = "Number of individual authors", main = "Gender of authors")
}

# Disciplinary background (bad data)
plotAuthorsDiscipline = function(authors) {
  tab <- table(authors$discipline, useNA = "ifany")
  names(tab)[is.na(names(tab))] <- "unknown"
  barplot(tab, ylab = "Number of individual authors", main = "Disciplinary background of authors")
}

# Most articles published
plotMostArticlesPublished <- function(authors) {
  threshold = quantile(authors$article_count, 0.9)
  authors$label = ifelse(authors$article_count > threshold, authors$full_name, NA)

  most_productive_authors <- authors %>%
    filter(!is.na(label)) %>%
    group_by(article_count)

  p <- ggplot(data = most_productive_authors, aes(x = label, y = article_count)) +
    geom_bar(aes(fill = label, x = reorder(label, article_count)), stat = "identity", show.legend = FALSE) +
    ggtitle("Authors with most articles published (including reviews)") +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
    ylab("Article Count") +
    xlab("") +
    geom_col() +
    coord_flip()
  p
}

plotAuthorsGenderTimeseries = function(articles) {
  gender_by_year = articles %>%
    mutate(year = format(date, "%Y")) %>%
    group_by(year) %>%
    count(gender) %>%
    pivot_wider(names_from = gender, values_from = n, values_fill = 0)
  gender_by_year$sum <- rowSums(gender_by_year[2:3])
  max_num_articles = max(gender_by_year$sum)
  gender_by_year$female_percent <- as.numeric(gender_by_year$female / (gender_by_year$male + gender_by_year$female) * max_num_articles)


  p <- gender_by_year[, 1:3] %>%
    pivot_longer(-year) %>%
    ggplot(aes(x = year, y = value, fill = name)) +
    ggtitle("Articles published with gender distribution") +
    geom_bar(stat = 'identity') +
    geom_line(
      data = gender_by_year[, -2:-4],
      mapping = aes(x = year, y = female_percent, group = 1),
      size = 1, color = "red", inherit.aes = FALSE
    ) +
    geom_smooth(
      data = gender_by_year[, -2:-4],
      mapping = aes(x = year, y = female_percent, group = 1),
      size = 1, color = "orange", inherit.aes = FALSE,
      method = "lm",
    ) +
    scale_y_continuous(
      sec.axis = sec_axis(
        trans = ~. / (max_num_articles),
        name = "Percentage of female authors",
        labels = function(b) { paste0(round(b * 100, 0), "%") }
      )
    ) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5),
          axis.text.y.right = element_text(color = "red")) +
    labs(x = "Year", y = "Articles", fill = 'Gender')
  p
}
```

Type any R code in the chunk, for example:
```{r}
mycars <- within(mtcars, { cyl <- ordered(cyl) })
mycars
```

Now, click the **Run** button on the chunk toolbar to [execute](https://www.jetbrains.com/help/pycharm/r-markdown.html#run-r-code) the chunk code. The result should be placed under the chunk.
Click the **Knit and Open Document** to build and preview an output.
