import requests
import json
import regex as re

def find_image_urls(text):
    """Extracts all image URLs from a given text"""
    url_pattern = re.compile(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\\(\\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+\.(?:jpg|gif|png|jpeg)")
    urls = re.findall(url_pattern, text)
    return urls

class HedgedocClient:

    BASE_URL = None
    
    def __init__(self, base_url) -> None:
        self.BASE_URL = base_url
        
    def base_url(self):
        return self.BASE_URL
    
    def upload_image(self, image_path):
        """
        Function to upload an image.
        The image_path parameter should be a string representing the file path to the image.
        Returns the URL of the uploaded image.
        """
        url = f'{self.BASE_URL}/uploadimage'
        headers = {
            'Content-Type': 'multipart/form-data',
        }
        with open(image_path, 'rb') as img_file:
            files = {
                'image': (image_path, img_file, 'image/png'),
            }
            
        upload_response = requests.post(url, headers=headers, files=files)
        
        if upload_response.status_code != 200:
            raise RuntimeError("Upload failed")
        
        return upload_response.json()['link']
        
    def upload_image_from_url(self, image_url):
        """
        Function to upload an image from a URL.
        The image_url parameter should be a string representing the URL of the image.
        Returns the URL of the uploaded image.
        """
        upload_url = f'{self.BASE_URL}/uploadimage'
        response = requests.get(image_url, stream=True)
        response.raise_for_status()
        filename = image_url.split("/")[-1]
        
        # We make use of the context manager to avoid storing the whole file in memory.
        with response:
            files = {'image': (filename, response.raw, response.headers.get('Content-type'))}
            upload_response = requests.post(upload_url, files=files)

        if upload_response.status_code != 200:
            raise RuntimeError("Upload failed")
        
        return upload_response.json()['link']


    def create_note(self, data):
        """
        Function to import markdown data into a new note.
        The note content will be the body of the received HTTP-request.
        """
        headers = {'Content-Type': 'text/markdown'}
        response = requests.post(f'{self.BASE_URL}/new', data=data, headers=headers)
        response.raise_for_status()
        
        if response.status_code != 200:
            raise RuntimeError("Request failed")
        
        return response.url

    def get_note_content(self, note_id):
        """
        Function to return the raw markdown content of a note.
        """
        response = requests.get(f'{self.BASE_URL}/{note_id}/download')
        response.raise_for_status()
        
        if response.status_code != 200:
            raise RuntimeError("Download failed")
        
        if response.text.startswith('<!DOCTYPE html>'):
            raise RuntimeError("Received HTML instead of MarkDown. Is the note publically readable?")
        
        # todo check if response is Markdown, if it is HTML most probably the note is not publically visible
        return response.text

    def get_note_metadata(self, note_id):
        """
        Function to return metadata about a note.
        This includes the title and description of the note as well as the creation date and viewcount.
        """
        response = requests.get(f'{self.BASE_URL}/{note_id}/info')
        response.raise_for_status()
        
        if response.status_code != 200:
            raise RuntimeError("Request failed")
        
        return response.json()
    
    def import_note(self, source_client, note_id):
        markdown = source_client.get_note_content(note_id)
        image_urls = find_image_urls(markdown)

        # copy images 
        if len(image_urls):
            for image_url in image_urls:
                new_image_url = self.upload_image_from_url(image_url)
                markdown = markdown.replace(image_url, new_image_url)

        # uplaod note with new image urls and return the url of the new note
        return self.create_note(markdown)    
        