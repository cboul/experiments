from hedgedoc_api import HedgedocClient, find_image_urls
import argparse
import os
from urllib.parse import urlparse
from platformdirs import user_cache_dir
import json
import time
import threading
from datetime import datetime
import keyboard

def parse_url(url):
    parsed_url = urlparse(url)
    base_url = f"{parsed_url.scheme}://{parsed_url.netloc}"
    doc_id = parsed_url.path.lstrip('/')  # remove leading '/' from path
    return base_url, doc_id

# run a function in an interval but stop when the escape key is pressed
stop_thread = False
def call_interval(func, interval):
    def wrapper():
        while True:
            if stop_thread:
                break
            time.sleep(interval)
            func()
    thread = threading.Thread(target=wrapper)
    thread.start()
    
def set_stop_thread(e):
    global stop_thread
    stop_thread = True

keyboard.on_press_key('esc', set_stop_thread)    
    
    
def parse_time(time_string):
    if time_string is None:
        return None
    return datetime.strptime(time_string, '%Y-%m-%dT%H:%M:%S.%fZ')


def flip_dict(input_dict):
    flipped_dict = {v: k for k, v in input_dict.items()}
    return flipped_dict

    
class HedgedocSyncClient(HedgedocClient):    

    doc = None
    doc_id = None
    doc_file = None
    meta_file = None
    meta_data = None
    verbose = None
    updatetime = None
    
    def __init__(self, basel_url, doc_id=None, verbose = False):
        self.cache_dir = user_cache_dir('hedgedoc-sync-local')
        self.doc_id = doc_id
        self.doc_file = os.path.join(self.cache_dir, f'{self.doc_id}.md')
        self.verbose = verbose
        self.meta_file = os.path.join(self.cache_dir, f'{self.doc_id}.json')
        super().__init__(basel_url)
        self.init()
    
    def init(self):
        # load or create metadata file
        if os.path.isfile(self.meta_file):
            with open(self.meta_file,'r', encoding='utf-8') as f:
                self.meta_data = json.load(f)
        else:
            os.makedirs(os.path.dirname(self.meta_file), exist_ok=True)
            self.meta_data = {
                "images": {}
            }
            self.save_metadata()
        
        # cache document content
        os.makedirs(os.path.dirname(self.doc_file), exist_ok=True)
        if self.doc_id is not None:
            markdown = self.get_note_content(self.doc_id)
            self.save_doc(markdown)
            
    def get_image_map(self):
        """Returns the reference to a dict containing the mapping of original to copied images"""
        return self.meta_data['images']
    
    def document_url(self):
        return f'{self.base_url()}/{self.doc_id}'

    def last_update_time(self):
        note_metadata = self.get_note_metadata(self.doc_id)
        updatetime = note_metadata.get('updatetime', None) or note_metadata.get('createtime', None) 
        return parse_time(updatetime) if updatetime else None

    def copy_images_and_update_urls(self, markdown, image_map = None):
        """checks if the image URLs in the markdown have already been copied and uploads the images if not. 
        Returns a copy of the markdown document with the replaced URLs and a dict mapping the original 
        image urls to the local copies."""
        
        if image_map is None:
            image_map = {}
        else:
            # make sure to have no side effects
            image_map = image_map.copy()
            
        image_urls = find_image_urls(markdown)
        total = len(image_urls)
        if total > 0:
            for index, image_url in enumerate(image_urls):
                new_image_url = image_map.get(image_url, None)
                if new_image_url is None:
                    if self.verbose:
                        print(f' - Uploading {index+1} of {total} images to {self.base_url()}')
                    new_image_url = self.upload_image_from_url(image_url)
                    image_map[image_url] = new_image_url
                elif self.verbose:
                    print(f' - Skipping already uploaded image {index+1} of {total} images')
                markdown = markdown.replace(image_url, new_image_url)   
        return markdown, image_map
        
    
    def save_doc(self, markdown=None):
        if markdown is not None:
            self.doc = markdown
        with open(self.doc_file, 'w', encoding='utf-8') as f:
            f.write(self.doc)
     
    def save_metadata(self):
        with open(self.meta_file, 'w', encoding='utf-8') as f:
            json.dump(self.meta_data, f)
        
        
    def update_from(self, other):
        if type(other) is not HedgedocSyncClient:
            raise ValueError("First argument must be a HedgedocSyncClient instance")
       
        if other.doc_id is None:
            raise ValueError("Cannot update as no doc_id set on the other client.")
        
        if self.verbose:
            print(f'Updating {self.document_url()} from {other.document_url()} ...')
        
        # update image mapping from other instance
        image_map = { v: k for k, v in other.get_image_map().items() if k.startswith(self.base_url()) }
        image_map = image_map.update(self.get_image_map())
        # replace imgae urls from other with those from self
        markdown, image_map = self.copy_images_and_update_urls(other.doc, image_map)
        self.get_image_map().update(image_map)

        # create note  if not exists
        if self.doc_id is None:
            new_note_url = self.create_note(markdown.encode())
            self.doc_id = os.path.basename(new_note_url)
            if self.verbose:
                print(f'Created copy at {self.document_url()}')
        else:
            print(f'Cannot update document because of current limitations of the HedgeDoc API.')
            print(f'Open {other.document_url()} and manually copy and paste content to {self.document_url()}.')
            
        # save the copy and the updated metadata
        self.save_doc(markdown)
        self.save_metadata()
        return self.doc_id
        

def main():
    parser = argparse.ArgumentParser(description='Sync two markdown documents hosted in different HedgeDoc instances.')

    # Add arguments to the parser
    parser.add_argument('command', help='The command to execute: checkout, checkin')
    parser.add_argument('source_url', help='URL of the source document.')
    parser.add_argument('-v', '--verbose', action='store_true', help='Provide verbose output.')
    parser.add_argument('-s', '--local-server-url', default='http://localhost:3000', help='The URL of the local HedgeDoc server.')

    # Parse the arguments
    args = parser.parse_args()
    source_base_url, source_doc_id = parse_url(args.source_url)
    target_base_url = args.local_server_url
    verbose = args.verbose
    
    # create the clients
    source = HedgedocSyncClient(source_base_url, source_doc_id, verbose=verbose)
    target_doc_id = source.meta_data.get('target_doc_id', None) 
    target = HedgedocSyncClient(target_base_url, target_doc_id, verbose=verbose)
    
    # handle the commands
    if args.command == "checkout":
        target_doc_id = target.update_from(source)
        source.meta_data['target_doc_id'] = target_doc_id
        source.save_metadata()
        print(f'Document has been checked out from {source.document_url()} to {target.document_url()}')
    elif args.command == "checkin":
        if target_doc_id is None: 
            print("Cannot check in, no target doc id is set")
            exit(1)
        source.update_from(target)
        print(f'Document has been checked in from {target.document_url()} to {source.document_url()}')
            
if __name__ == "__main__":
    main()