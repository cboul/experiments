# based on code written by GPT-4
import csv
import os
import mwclient
import os.path
import requests
import pandas as pd
import textwrap
import re

from pywikibot import Claim, WbTime, ItemPage, PropertyPage, Site
from datetime import datetime
from dotenv import load_dotenv

load_dotenv()


def generate_sparql_query(fullName, property_labels_to_ids, language='en', qid=None, include_description=False):
    """
    Query WikiData for the properties of the given person listed in the given property map,
    either by fullName or QID. When a QID is provided, ?itemLabel is not included in the query.
    :param fullName: Name of the person to query
    :param property_labels_to_ids: Dictionary mapping property labels to WikiData property IDs
    :param language: Language code for the query results
    :param qid: WikiData entity ID (QID) for the person
    :return: SPARQL query string
    """

    selectClause = "SELECT DISTINCT ?item"
    groupByClause = "GROUP BY ?item"

    if qid:
        #selectClause = "SELECT DISTINCT ?item"
        itemConstraint = f"BIND(wd:{qid} AS ?item)."
        #groupByClause = "GROUP BY ?item"
    else:
        #selectClause = "SELECT DISTINCT ?item"
        itemConstraint = f'?item wdt:P31 wd:Q5; rdfs:label "{fullName}"@{language} .'
        #groupByClause = "GROUP BY ?item"

    if include_description:
        selectClause += " ?itemdesc"
        itemConstraint += f'''
                OPTIONAL {{ ?item schema:description ?itemdesc. FILTER(LANG(?itemdesc) = "{language}") }}'''
        groupByClause += " ?itemdesc"

    for label, pid in property_labels_to_ids.items():
        # add to property selection
        if label.endswith('Name'):
            selectClause += f'''
                (GROUP_CONCAT(DISTINCT ?{label}Value; separator=" ") AS ?{label})'''
        else:
            selectClause += f'''
                (SAMPLE(?{label}) AS ?{label})'''

        # add to item constraint
        if label.endswith("_id") or label.startswith("image") or label.startswith("date"):
            itemConstraint += f"""
                OPTIONAL {{ ?item wdt:{pid} ?{label}. }}"""
        elif label.endswith("Name"):
            itemConstraint += f"""
                OPTIONAL {{
                ?item p:{pid} ?{label}Statement.
                    ?{label}Statement ps:{pid} ?{label}.
                    OPTIONAL {{ ?{label}Statement pq:P1545 ?order. }}
                    OPTIONAL {{
                ?{label} rdfs:label ?{label}Label.
                      FILTER(LANG(?{label}Label) = "{language}")
                    }}
                    BIND(COALESCE(?{label}Label, STR(?{label})) AS ?{label}Value)
                  }}"""
        else:
            itemConstraint += f"""
                OPTIONAL {{ ?item wdt:{pid} ?{label}Id . ?{label}Id rdfs:label ?{label} FILTER(LANG(?{label}) = "{language}") . }}"""

    query = textwrap.dedent(f"""
    {selectClause} 
    WHERE {{
        {itemConstraint}
    }}
    {groupByClause}
    """)

    return query


def query_wikidata(fullName, property_map, language='en', qid=None, debug=False, include_description=False):
    SPARQL_ENDPOINT = "https://query.wikidata.org/sparql"
    query = generate_sparql_query(fullName, property_map, language, qid=qid, include_description=include_description)
    if debug:
        print(query)
    headers = {'User-Agent': 'Mozilla/5.0', 'Accept': 'application/json'}
    response = requests.get(SPARQL_ENDPOINT, headers=headers, params={'query': query, 'format': 'json'})

    if response.status_code != 200:
        print(query)
        response.raise_for_status()

    results = response.json()['results']['bindings']

    if not results:
        return []

    for i, result in enumerate(results):
        # Initialize with fullName to ensure it appears first
        data = {
            'fullName': fullName
        }

        labels = list(property_map.keys())
        if include_description:
            labels.append('itemdesc')

        for label in labels:
            if label in result:
                value = result[label]['value']
                data[label] = value
            else:
                data[label] = None

        # add qid and item URI
        data['qid'] = os.path.basename(result['item']['value'])
        data['wikidata_url'] = result['item']['value']
        results[i] = data

    return results


def get_wikipedia_links(qid, languages):
    """
    Fetch Wikipedia links for a given Wikidata QID and a list of languages.

    Parameters:
    - qid (str): The QID of the Wikidata item.
    - languages (list): A list of language codes (e.g., ['en', 'de']).

    Returns:
    - dict: A dictionary with languages as keys and Wikipedia URLs as values.
    """
    url = "https://www.wikidata.org/w/api.php"
    params = {
        "action": "wbgetentities",
        "ids": qid,
        "props": "sitelinks/urls",
        "format": "json"
    }

    response = requests.get(url, params=params)
    data = response.json()
    links = {}
    if "entities" in data and qid in data["entities"]:
        sitelinks = data["entities"][qid].get("sitelinks", {})
        for lang in languages:
            sitekey = f"{lang}wiki"
            if sitekey in sitelinks:
                siteLinkData = sitelinks.get(sitekey)
                if 'url' in siteLinkData:
                    links[lang] = siteLinkData.get('url')
                else:
                    # Use the 'title' key and construct the URL manually
                    title = sitelinks[sitekey]["title"]
                    links[lang] = f"https://{lang}.wikipedia.org/wiki/{requests.utils.quote(title)}"
            else:
                links[lang] = None  # Or use '' to represent absence of link

    return links


def extract_name_qid_with_regex(strings):
    pattern = re.compile(r'^(.+?)(?: \(?Q(\d+)\)?)? *$')
    result = []
    for s in strings:
        if match := pattern.search(s.strip()):
            name = match.group(1).strip()
            qid = 'Q' + match.group(2) if match.group(2) else None
            result.append((name, qid))
    return result


def get_person_info_from_wikidata(names: list,
                                  property_map: dict,
                                  languages: list = None,
                                  debug=False,
                                  include_description=False) -> pd.DataFrame:
    """
    Given a list of "Name (QID)" strings, return the property values stored in wikidata, including wikipedia page links
    Args:
        names:
            a list of strings in the format "Name (QID)". "(QID") is optional. If left jsonl, the result will contain all
            items having that name
        property_map:
            a dict mapping names of the property to PIDs
        languages:
            a list of languages for which to retrieve the wikipedia page URL, if it exists
        debug:
            if true, output debug information

    Returns:
        A dataframe with the property names as column names

    """

    if languages is None:
        languages = ['en', 'de']

    language = languages[0]

    all_data = []
    print('Retrieving scholar data...')
    for name, qid in extract_name_qid_with_regex(names):
        all_data += query_wikidata(name,
                                   property_map=property_map,
                                   language=language,
                                   qid=qid,
                                   include_description=include_description,
                                   debug=debug)

    # Ensure fullName appears first by reordering columns based on property_labels_to_ids keys
    columns = (['fullName', 'qid'] +
               (['itemdesc'] if include_description else []) +
               list(property_map.keys()) +
               ['wikidata_url'] + [f'wikipedia_{l}' for l in languages])

    if len(all_data) > 0:
        df = pd.DataFrame(all_data, columns=columns, dtype=str)
        # Add wikipedia links
        print("Retrieving wikipedia URLs...")
        # For each QID in the DataFrame, fetch Wikipedia links for all languages and update the DataFrame accordingly
        for index, row in df.iterrows():
            qid = row['qid']
            links = get_wikipedia_links(qid, languages)

            # Update the DataFrame directly with the fetched links for each language
            for language in languages:
                df.at[index, f'wikipedia_{language}'] = links.get(language, None)
    else:
        df = pd.DataFrame(columns=columns, dtype=str)
    return df


def claim_to_string(claim):
    subject_qid = claim.on_item.id
    predicate_pid = claim.getID()

    # Object QID, assuming the target is a Wikidata item
    # Note: This simplification assumes the claim's target is an item.
    # For other target types (e.g., quantities, strings), additional handling is needed.
    if isinstance(claim.getTarget(), ItemPage):
        object_qid = claim.getTarget().id
    else:
        # Placeholder or additional logic for non-ItemPage targets
        object_qid = 'N/A'  # This could be expanded to handle other types of targets

    return f"({subject_qid})-[{predicate_pid}]-({object_qid})"


# Function to check if a specific time qualifier exists
def time_qualifier_exists(claim, qualifier_pid, year_value):
    for qualifier in claim.qualifiers.get(qualifier_pid, []):
        qualifier_date = qualifier.getTarget()
        if qualifier_date.year == year_value:
            print(f'Time qualifier {qualifier_pid} with value {year_value} already exists on {claim_to_string(claim)}.')
            return True
    return False


def add_time_qualifiers(repo, claim, start_time, end_time):
    qualifiers = []

    if (start_time and end_time) and (start_time == end_time):
        if not time_qualifier_exists(claim, 'P585', int(start_time)):
            point_in_time_qualifier = Claim(repo, 'P585')
            point_in_time_qualifier.setTarget(WbTime(year=int(start_time)))
            claim.addQualifier(point_in_time_qualifier, summary='Adding point in time')
            print(f'Added point_in_time qualifier to {claim_to_string(claim)}')
            qualifiers.append(point_in_time_qualifier)

    else:
        if start_time and not time_qualifier_exists(claim, 'P580', int(start_time)):
            start_time_qualifier = Claim(repo, 'P580')
            start_time_qualifier.setTarget(WbTime(year=int(start_time)))
            claim.addQualifier(start_time_qualifier, summary='Adding start time')
            print(f'Added start_time qualifier to {claim_to_string(claim)}')
            qualifiers.append(start_time_qualifier)

        if end_time and not time_qualifier_exists(claim, 'P582', int(end_time)):
            end_time_qualifier = Claim(repo, 'P582')
            end_time_qualifier.setTarget(WbTime(year=int(end_time)))
            claim.addQualifier(end_time_qualifier, summary='Adding end time')
            print(f'Added end_time qualifier to {claim_to_string(claim)}')
            qualifiers.append(end_time_qualifier)

    return qualifiers


# Function to check if a reference with the given URL already exists on the claim
def reference_url_exists(claim, url):
    for source in claim.getSources():
        if 'P4656' in source or 'P854' in source:  # Check both Wikimedia import URL and reference URL
            for prop in source.get('P4656', []) + source.get('P854', []):
                if prop.getTarget() == url:
                    print(f'Source URL {url} already exists on {claim_to_string(claim)}.')
                    return True
    return False


def qualifier_exists(claim, qualifier_property_id, target):
    for existing_qualifier in claim.qualifiers.get(qualifier_property_id, []):
        if existing_qualifier.getTarget() == target:
            print(
                f'Qualifier {qualifier_property_id} with value {target.getID()} already exists on {claim_to_string(claim)}.')
            return True
    return False


def add_reference(repo, claim, reference_url, retrieved_at_time, qualifiers=None):
    sources = []
    if reference_url and not reference_url_exists(claim, reference_url):
        # Determine whether the URL is a Wikipedia URL or another type of URL
        property_id = 'P4656' if 'wikipedia.org' in reference_url else 'P854'

        # Create the reference claim
        source_claim = Claim(repo, property_id)
        source_claim.setTarget(reference_url)
        sources.append(source_claim)

        # Create the 'retrieved at' claim
        retrieved_at_claim = Claim(repo, 'P813')
        retrieved_at_target = WbTime(year=retrieved_at_time.year, month=retrieved_at_time.month,
                                     day=retrieved_at_time.day)
        retrieved_at_claim.setTarget(retrieved_at_target)
        sources.append(retrieved_at_claim)

    # If a qualifier has been passed for which this reference is the source, add it
    if qualifiers:
        for qualifier in qualifiers:
            supports_qualifier_claim = Claim(repo, 'P10551')  # "supports qualifier"
            site = Site("wikidata", "wikidata")
            property_page = PropertyPage(site, qualifier.getID())
            if not qualifier_exists(claim, 'P10551', property_page):
                supports_qualifier_claim.setTarget(property_page)
                sources.append(supports_qualifier_claim)

    # Add the references to the claim
    if len(sources) > 0:
        claim.addSources(sources, summary='Adding reference and retrieved at date')
        print(f'Added references to {claim_to_string(claim)}')

    return sources


# main function
def update_wikidata_from_csv(file_path):
    site = Site("wikidata", "wikidata")
    repo = site.data_repository()

    previous_object_qid = None
    previous_claim = None

    with open(file_path, newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            print("----------")
            subject_qid = row['subject-qid']
            pid = row['pid']
            object_qid = row['object-qid']
            start_time = row['start_time']
            end_time = row['end_time']
            reference_url = row['reference_url']

            # If the new subject is identical to the old object, refine the previous claim
            if subject_qid == previous_object_qid and previous_claim:
                claim = previous_claim
                print(f'Refining {claim_to_string(claim)}')
            else:
                item = ItemPage(repo, subject_qid)
                item.get()

                # Check if the claim already exists
                claim_exists = False
                for claim in item.claims.get(pid, []):
                    if claim.getTarget().id == object_qid:
                        claim_exists = True
                        print(f'{claim_to_string(claim)} exists.')
                        break

                if not claim_exists:
                    claim = Claim(repo, pid)
                    target = ItemPage(repo, object_qid)
                    claim.setTarget(target)
                    item.addClaim(claim)
                    print(f'Created {claim_to_string(claim)}')

            # start_time and end_time
            qualifiers = add_time_qualifiers(repo, claim, start_time, end_time)

            # references
            retrieved_at_time = datetime.utcnow()
            add_reference(repo, claim, reference_url, retrieved_at_time, qualifiers)

            # Remember the object and claim for the next iteration
            previous_object_qid = object_qid
            previous_claim = claim


def get_wikipedia_page_data(pageTitle: str, language="en"):
    user_agent = os.getenv('USER_AGENT')
    site = mwclient.Site(f'{language}.wikipedia.org', clients_useragent=user_agent)
    page = site.pages[pageTitle]
    if not page.exists:
        return None

    return {
        'page': page,
        'revision': page.revision,
        'url': f'{site.host}/wiki/{pageTitle.replace(" ", "_")}?oldid={page.revision}',
        'content': page.text()
    }


# Function to convert URLs to HTML links
def make_clickable(val, name):
    if val:
        return f'<a target="_blank" href="{val}">{name}</a>'
    else:
        return ""


def format_name(name, date_birth, date_death):
    return f'{name} ({"" if pd.isna(date_birth) else date_birth.year}-{"" if pd.isna(date_death) else date_death.year})'

def format_language_codes(name):
    # Find all occurrences of language codes (e.g., _en, _de) and transform them to uppercase within parentheses
    return re.sub(r'(.*?)_([a-z]{2})', lambda m: f" ({m.group(2).upper()})", name)

def create_styled_table(df: pd.DataFrame, include_rows:list):
    df = df.copy()
    df['fullName'] = df.apply(lambda r: format_name(r['fullName'], r['dateOfBirth'], r['dateOfDeath']), axis=1)
    df = df[include_rows]
    for col in df.columns:
        if col.startswith('wiki'):
            if col.startswith('wikipedia'):
                link_name = 'WP ' + format_language_codes(col)
            else:
                link_name = "Wikidata"
            df.loc[:, col] = df.loc[:, col].apply(make_clickable, name=link_name)

    return df
