import io
from langchain_core.prompts import ChatPromptTemplate
from langchain_core.output_parsers import StrOutputParser
import pandas as pd
from dotenv import load_dotenv

load_dotenv()

def extract_to_csv(model, template, debug=False, **params):
    prompt = ChatPromptTemplate.from_template(template)
    parser = StrOutputParser()
    chain = ( prompt | model | parser )
    response = chain.invoke(params)
    if debug:
        print(response)
    data = io.StringIO(response)
    return pd.read_csv(data, dtype={'start_time': str, 'end_time': str})
