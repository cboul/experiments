#!/usr/bin/env bash

convert_from=$1
convert_to=$2
input_dir=${convert_from/bibtex/bib}
output_dir=${convert_to/bibtex/bib}
input_ext=$input_dir
output_ext=${output_dir/csljson/csl.json}
for input_path in $input_dir/*.$input_ext
do
  out_file=$(basename ${input_path/\.$input_ext/.$output_ext})
  echo "Running citeproc to convert $input_path to $output_dir/$out_file..."
  pandoc -f "$convert_from" -t "$convert_to" -o "$output_dir/$out_file" "$input_path" 2>&1
done