import xml.etree.ElementTree as ET
from lxml import etree
import regex as re
from nameparser import HumanName
from lib.xml import remove_whitespace


def even_num_brackets(string: str):
    """
    Simple heuristic to determine if string contains an even number of round and square brackets,
    so that if not, trailing or leading brackets will be removed.
    """
    return ((string.endswith(")") and string.count(")") == string.count("("))
            or (string.endswith("]") and string.count("]") == string.count("[")))


def remove_punctuation(text, keep_trailing_chars="?!"):
    """This removes leading and trailing punctuation using very simple rules for German and English"""
    start, end = 0, len(text)
    while start < len(text) and re.match("\p{P}", text[start]) and text[end - 1]:
        start += 1
    while end > start and re.match("\p{P}", text[end - 1]) and not even_num_brackets(text[start:end]) and text[
        end - 1] not in keep_trailing_chars:
        end -= 1
    return text[start:end].strip()


def remove_punctuation2(text):
    """same as remove_punctuation, but keep trailing periods."""
    return remove_punctuation(text, "?!.")


def clean_editor(text):
    text = re.sub(r'^in(:| )', '', remove_punctuation(text), flags=re.IGNORECASE)
    text = re.sub(r'\(?(hrsg\. v\.|hg\. v|hrsg\.|ed\.|eds\.)\)?', '', text, flags=re.IGNORECASE)
    return text.strip()


def clean_container(text):
    return remove_punctuation(re.sub(r'^(in|aus|from)(:| )', '', text.strip(), flags=re.IGNORECASE))


def extract_page_range(text):
    match = re.match(r'(\p{Alnum}+)(?: *\p{Pd} *(\p{Alnum}+))?', text)
    attributes = {"unit": "page"}
    if match:
        from_page = match.group(1)
        to_page = match.group(2)
        attributes.update({"from": from_page})
        if to_page is not None:
            attributes.update({"to": to_page})
    return attributes


def process_range(text):
    cleaned_text = re.sub(r'^(S\.|p\.|pp\.)', '', text.strip(), flags=re.IGNORECASE)
    cleaned_text = re.sub(r'(ff?\.|seqq?\.)$', '', cleaned_text.strip(), flags=re.IGNORECASE)
    text = remove_punctuation(text, keep_trailing_chars='.')
    attributes = extract_page_range(cleaned_text)
    return (text, attributes)


def handle_pages(text, bibl, tag, preserve):
    # todo: handle "at"/"p" -> citedRange
    # todo: keep pp. etc within the tags but parse `from` and `to` information
    if text == "": return
    # split by comma or semicolon along with any trailing spaces
    ranges = re.split(r'([,;] *)', text)
    # initialize an empty list to store results
    page_locators = []
    # loop through indices with a step of 2
    for i in range(0, len(ranges) - 1, 2):
        # combine current element with the next one (which is a separator), and append to the list
        page_locators.append(ranges[i] + ranges[i + 1])
    # if the input text doesn't end with a separator, add the last element
    if text[-1] not in [',', ';']:
        page_locators.append(ranges[-1])
    for page_locator in page_locators:
        add_node(bibl, tag, page_locator, clean_func=process_range, preserve=preserve)


def clean_volume(text):
    # TODO: handle issue
    cleaned_text = re.sub(r'(volume|vol\.?|bd\.?|band) ', '', text.strip(), flags=re.IGNORECASE)
    text = remove_punctuation(text)
    return text, {"unit": "volume", "from": cleaned_text, "to": cleaned_text}


def clean_issue(text):
    cleaned_text = re.sub(r'(issue|heft|no\.?) ', '', text.strip(), flags=re.IGNORECASE)
    text = remove_punctuation(text)
    return text, {"unit": "issue", "from": cleaned_text, "to": cleaned_text}


def extract_text_in_parentheses(text):
    match = re.search(r'(.*?)\s*(\(.*?\))', text)
    if match:
        return match.group(1), match.group(2)
    else:
        return text, None


def extract_date(text):
    # todo: add when="" attribute and allow arbitrary dates
    m = re.search(r'[12][0-9]{3}', text)
    return m.group(0) if m else None


def find_string(string, container):
    start = container.find(string)
    if start > -1:
        end = start + len(string)
        return start, end
    raise ValueError(f"Could not find '{string}' in '{container}'")


def add_node(parent, tag, text="", attributes=None, clean_func=None, preserve=False):
    """
    TODO convert to lxml
    Adds a child node to the parent, optionally adding text and attributes.
    If a clean_func is passed, the text is set after applying the function to it.
    If the `preserve` flag is True, the removed preceding or trailing text is preserved in the xml, outside of the node content
    """
    node = ET.SubElement(parent, tag, (attributes or {}))
    if clean_func:
        cleaned_text = clean_func(text)
        if type(cleaned_text) is tuple:
            # in a tuple result, the first element is the text and the second node attributes
            for key, value in cleaned_text[1].items():
                node.set(key, value)
            cleaned_text = cleaned_text[0]
        if preserve:
            start, end = find_string(cleaned_text, text)
            prefix, suffix = text[:start], text[end:]
            if prefix != "" and len(parent) > 1:
                prev_sibling = parent[-2]
                prev_tail = (prev_sibling.tail or '')
                new_prev_tail = f'{prev_tail} {prefix}'.strip()
                prev_sibling.tail = new_prev_tail
            node.text = cleaned_text
            if suffix != "":
                node.tail = suffix
    else:
        node.text = text
    return node


def create_tei_root():
    return ET.Element('TEI', {
        'xmlns': "http://www.tei-c.org/ns/1.0"
    })


def create_tei_header(tei_root, title):
    tei_header = add_node(tei_root, 'teiHeader')
    file_desc = add_node(tei_header, 'fileDesc')
    title_stmt = add_node(file_desc, 'titleStmt')
    add_node(title_stmt, 'title', title)
    publication_stmt = add_node(file_desc, 'publicationStmt')
    add_node(publication_stmt, 'publisher', 'mpilhlt')
    source_desc = add_node(file_desc, 'sourceDesc')
    add_node(source_desc, 'p', title)
    return tei_header


def create_body(text_root):
    body = ET.SubElement(text_root, 'body')
    add_node(body, 'p', 'The article text is not part of this document')
    return body


def split_creators(text: str, bibl, tag, clean_func, preserve):
    """
    given a string containing one or more names of creators (authors, editors, etc.), split them into individual
    names and add them to the parent bibl element
    Args:
        text:
        bibl:
        tag:
        clean_func:
        preserve:

    Returns:

    """
    sep_regex = r'[;&/]| and | und '
    creators = re.split(sep_regex, text)
    seperators = re.findall(sep_regex, text)
    for creator in creators:
        # <author>/<editor>
        creator_node = add_node(bibl, tag, creator, clean_func=clean_func, preserve=preserve)
        # <persName>
        name = HumanName(creator_node.text)
        creator_node.text = ''
        pers_name = add_node(creator_node, 'persName')
        inv_map = {v: k for k, v in name.as_dict(False).items()}
        if len(name) == 1:
            add_node(pers_name, 'surname', list(name)[0])
        else:
            names_list = []
            for name_part in list(name):
                idx = text.index(name_part)
                type = inv_map[name_part]
                match type:
                    case 'last':
                        # <surname>
                        names_list.append((idx, 'surname', name_part, None))
                    case 'first' | 'middle':
                        # <forename>
                        names_list.append((idx, 'forename', name_part, type))
            # sort by index
            names_list.sort()
            for n in names_list:
                tag = n[1]
                name = n[2]
                type = n[3]
                if type:
                    attr = {'type': type}
                else:
                    attr = None
                name_part_element = add_node(pers_name, tag, name, attr)
                # re-attach trailing comma to surname if original text had it
                if tag == 'surname' and n != names_list[-1]:
                    if f'{name},' in text:
                        name_part_element.tail = ','

        # re-attach other separators
        if len(seperators):
            creator_node.tail = seperators.pop(0).strip()


def anystyle_to_tei(input_xml_path, id, preserve=False):
    ns = {"tei": "http://www.tei-c.org/ns/1.0"}
    anystyle_root = ET.parse(input_xml_path).getroot()
    tei_root = create_tei_root()
    create_tei_header(tei_root, title=id)
    text_root = add_node(tei_root, 'text')
    body = create_body(text_root)
    # <listBibl> element for <bibl> elements that are not in footnotes, such as a bibliography
    list_bibl_elem = None
    # the parent node all bibl get attached to
    current_bibl_parent = None
    # iterate over all sequences (=footnotes) and translate into TEI equivalents
    for sequence in anystyle_root.findall('sequence'):
        # if the sequence contains a citation-number, create a new <note>
        if (cn := sequence.findall('citation-number')):
            footnote_number = cn[0].text
            attributes = {
                'n': footnote_number,
                'type': 'footnote',
                'place': 'bottom'
            }
            current_bibl_parent = add_node(body, 'note', attributes=attributes, clean_func=remove_punctuation,
                                           preserve=preserve)
        else:
            # otherwise create a <listBibl> element
            if list_bibl_elem is None or current_bibl_parent is None or current_bibl_parent.tag == 'note':
                list_bibl_elem = add_node(body, 'listBibl', attributes={'type': 'bibliography'})
            current_bibl_parent = list_bibl_elem
        bibl = None
        for child in sequence:
            tag = child.tag
            text = child.text

            if tag == "citation-number":
                # this has already been taken care of
                continue

            if bibl is None:
                # if we do not have a bibl element yet, create it
                bibl = ET.SubElement(current_bibl_parent, 'bibl')

            elif ((bibl.find(tag) and tag != "note")
                  # if it exists, check if tag already exists in the current element
                  or tag in ['signal', 'legal-ref']
                  # or tag belongs to a specific groups that signal a separate reference or specific tags follow a date field
                  or (tag in ["author", "editor", "authority"] and bibl.find('date'))):
                # then switch to a new element
                bibl = ET.SubElement(current_bibl_parent, 'bibl')

                # if we have a new element and are not in a footnote, this is an in-text reference list and not a bibliography
                if current_bibl_parent.tag == "listBibl":
                    current_bibl_parent.attrib['type'] = 'inText'

            match tag.lower():
                case 'author':
                    split_creators(text, bibl, 'author', clean_func=remove_punctuation, preserve=preserve)
                case 'authority':
                    split_creators(text, bibl, 'publisher', clean_func=remove_punctuation, preserve=preserve)
                case 'backref':
                    add_node(bibl, 'ref', text, clean_func=remove_punctuation2, preserve=preserve)
                case 'container-title':
                    add_node(bibl, 'title', text, {'level': 'm'}, clean_func=clean_container, preserve=preserve)
                case 'collection-title':
                    add_node(bibl, 'title', text, {'level': 's'}, clean_func=clean_container, preserve=preserve)
                case 'date':
                    add_node(bibl, 'date', text, clean_func=extract_date, preserve=preserve)
                case 'doi':
                    add_node(bibl, 'idno', text, {'type': 'DOI'})
                case 'edition':
                    add_node(bibl, 'edition', text, clean_func=remove_punctuation2, preserve=preserve)
                case 'editor':
                    split_creators(text, bibl, 'editor', clean_func=clean_editor, preserve=preserve)
                case 'id':
                    # arbitrary id number
                    add_node(bibl, 'idno', text)
                case 'ignore':
                    continue
                case 'issue':
                    add_node(bibl, 'biblScope', text, clean_func=clean_issue, preserve=preserve)
                case 'location':
                    add_node(bibl, 'pubPlace', text, clean_func=remove_punctuation, preserve=preserve)
                case 'note':
                    add_node(bibl, 'seg', text, {'type': 'comment'}, clean_func=remove_punctuation, preserve=preserve)
                case 'journal':
                    add_node(bibl, 'title', text, {'level': 'j'}, clean_func=clean_container, preserve=preserve)
                case 'legal-ref':
                    add_node(bibl, 'idno', text, {'type': 'caseNumber'}, clean_func=remove_punctuation,
                             preserve=preserve)
                case 'pages':
                    # todo: if we have "ff." or "seqq.", it is most probably a citedRange rather than a biblScope
                    # also, when pages follow book metadata such as publisher or location
                    if bibl[-1].tag == "ref":
                        handle_pages(text, bibl, 'citedRange', preserve=preserve)
                    else:
                        pages, cited_range = extract_text_in_parentheses(text)
                        handle_pages(pages, bibl, 'biblScope', preserve=preserve)
                        if cited_range:
                            handle_pages(cited_range, bibl, 'citedRange', preserve=preserve)
                case 'publisher':
                    add_node(bibl, 'publisher', text)
                case 'signal':
                    add_node(bibl, 'seg', text, {'type': 'signal'})
                case 'title':
                    # todo: must be level="m" if no container element!
                    level = 'a' if sequence.find('container-title') or sequence.find('journal') else 'm'
                    add_node(bibl, 'title', text, {'level': level}, clean_func=remove_punctuation2, preserve=preserve)
                case 'url':
                    add_node(bibl, 'ptr', text, {'type': 'web'}, clean_func=remove_punctuation, preserve=preserve)
                case 'volume':
                    volume, issue = extract_text_in_parentheses(text)
                    add_node(bibl, 'biblScope', volume, clean_func=clean_volume, preserve=preserve)
                    if issue:
                        add_node(bibl, 'biblScope', issue, clean_func=clean_issue, preserve=preserve)

        # add trailing period if missing
        if bibl[-1].tail is None:
            bibl[-1].tail=''
        if sequence[-1].text.endswith('.') and not bibl[-1].tail.endswith('.'):
            bibl[-1].tail += '.'


    # convert to lxml
    tei_root_str = ET.tostring(tei_root, encoding='unicode')
    lxml_root = etree.fromstring(tei_root_str)

    # Remove empty elements
    for empty_element in lxml_root.xpath('//*[not(node())]', namespaces=ns):
        # print(f'    - removed empty {empty_element.tag}')
        empty_element.getparent().remove(empty_element)

    # Convert back to xml.etree.ElementTree
    lxml_str = etree.tostring(lxml_root, encoding="unicode")

    # Register Namespace
    ET.register_namespace('', 'http://www.tei-c.org/ns/1.0')
    final_root = ET.fromstring(lxml_str)

    return ET.tostring(final_root, 'unicode')


def tei_to_input(tei_xml_doc):
    """
    Extract the original footnote strings from the <note> elements in a given TEI document and return a list of strings
    """
    root = etree.fromstring(tei_xml_doc)
    ref_list = []
    ns = {"tei": "http://www.tei-c.org/ns/1.0"}
    # iterate over the <note type="footnote"> and <listBibl> elements
    for xpath in ['*//tei:listBibl[@type="inText"]',
                  '*//tei:note[@type="footnote"]',
                  '*//tei:listBibl[@type="bibliography"]']:
        for element in root.findall(xpath, ns):
            localname = etree.QName(element).localname
            type = element.attrib['type'] if 'type' in element.attrib else None
            ref_parts = []
            if 'n' in element.attrib:
                ref_parts.append(element.attrib['n'])
            # iterate over the <bibl> elements
            for bibl in element.findall('tei:bibl', ns):
                # extract the text without xml tags, still contains all (collapsed) whitespace
                text = etree.tostring(bibl, method="text", encoding='utf-8').decode()
                text = remove_whitespace(text)
                ref_parts.append(text)
            if localname == "listBibl" and type != "inText":
                ref_list += ref_parts
            elif localname == "note" or (localname == "listBibl" and type == "inText"):
                ref_list.append(remove_whitespace(" ".join(ref_parts)))
    return ref_list