from lxml import etree
import os
from glob import glob
from copy import deepcopy
from IPython.display import display, Markdown
from pathlib import Path
from lxml import etree
import regex as re
import html
import textwrap

# namespaces
tei_namespace = "http://www.tei-c.org/ns/1.0"
xml_namespace = "http://www.w3.org/XML/1998/namespace"
ns = {  
    "tei": tei_namespace,
    "xml": xml_namespace 
}
id_attrib = f'{{{xml_namespace}}}id'

def prepare_bibl(dir_path):
    for file_path in glob(f'{dir_path}/*.xml'):
        print(f' - Processing {file_path}')
        tree = etree.parse(file_path)
        move_seg_out_of_bibl(tree)
        add_id_to_bibl(tree)
        # fix incorrect whitespace    
        for elem in tree.xpath(r'//*'):
            if type(elem.tail) is str and elem.tail != "": 
                fix_tail(elem)
        pretty_tree = etree.tostring(tree, pretty_print=True, encoding='utf-8')
        with open(file_path, 'wb') as file:
            file.write(pretty_tree)


def add_id_to_bibl(tree: etree._Element):
    """Add id to bibl"""
    n = 1
    for element in tree.xpath('*//tei:bibl', namespaces=ns):
        if 'n' in element.attrib:
            del element.attrib['n']
        element.attrib[id_attrib] = f'bibl-{str(n)}'
        n += 1


def move_seg_out_of_bibl(root: etree._Element):
    """Move <seg> elements at the beginning or end of <bibl> out of the node"""
    nodes_with_bibl_children = root.xpath('//*[tei:bibl]', namespaces=ns)
    for node in nodes_with_bibl_children:
        bibls = node.xpath('./tei:bibl', namespaces=ns)
        for bibl in bibls:
            children = bibl.getchildren()
            # check if the first/last child are tei:seg
            while (segs_to_move := bibl.xpath('./tei:seg[(@type="signal" or @type="comment") and (position() = 1 or position() = last())]', namespaces=ns)):
                for seg in segs_to_move:
                    if children.index(seg) == 0:
                        node.insert(node.index(bibl), seg)
                    else:
                        node.insert(node.index(bibl) + 1, seg)
                        


def remove_whitespace(text):
    # we need to remove the whitespace that comes with the indentation of pretty-printed xml
    text = re.sub(r'\n\s*', ' ', text)

    # reduce double spaces to one
    while re.search(r'\s\s', text):
        text = re.sub(r'\s\s', ' ', text)

    # escape character sequences which would be corrupted by whitespace removal rules
    text = re.sub(r'\. \. \.', '[!spaced_elipsis!]', text)

    # fix issues with whitespace before punctuation
    text = re.sub(r' ([.;,!?%:])( |$)', r'\1 ', text)
    # opening and closing punctutation, such as brackets
    text = re.sub(r'(\p{Ps}) ', r'\1', text)
    text = re.sub(r' (\p{Pe})', r'\1', text)
    # opening and closing quotes
    text = re.sub(r'(\p{Pi}) ', r'\1', text)
    text = re.sub(r' (\p{Pf})', r'\1', text)
    # slash
    text = re.sub(r' ?/ ?', r'/', text)

    # restore sequences
    text = text.replace('[!spaced_elipsis!]', r'. . .')
    return text.strip()

def indentation_level(element):
    level = 0
    while element is not None:
        element = element.getparent()
        level += 1
    return level - 1 

def fix_tail(elem: etree._Element, indentation="  "):
    tail = elem.tail
    # normalize line endings
    tail = re.sub(r'\r\n', '\n', tail)
    # fix issues with whitespace before punctuation
    tail = re.sub(r'(\s*\n\s+)([.;,!?%:/])', r'\2', tail)
    # opening and closing punctutation, such as brackets
    tail = re.sub(r'(\p{Ps})(\s*\n\s+)', r'\1', tail)
    tail = re.sub(r'(\s*\n\s+)(\p{Pe})', r'\2', tail)
    # opening and closing quotes
    tail = re.sub(r'(\p{Pi})(\s*\n\s+)', r'\1', tail)
    tail = re.sub(r'(\s*\n\s+)(\p{Pf})', r'\2', tail)
    # in tails without line break but with whitespace, replace normalized whitespace with linebreak
    if '\n' not in tail and re.match(r'\s', tail) is not None:
        tail = re.sub(r'\s+', r' ', tail).strip()
        p = tail.split(" ")
        s = '\n' + indentation_level(elem) * indentation
        tail = s.join(p) 
    elem.tail = tail
    
    
def prettify_content(elem: etree._Element, indentation="    ", width=120):
    text = re.sub(r'\s+', ' ', elem.text, flags=re.MULTILINE)
    lines = textwrap.wrap(text, width=width)
    indent = indentation_level(elem) * indentation
    lines = [ indent +  indentation + line for line in lines ]
    lines.insert(0, "")
    lines.append(indent)
    elem.text = '\n'.join(lines)
    
    
def fix_indentation(elem, level=0, indentation="    "):
    indent = "\n" + level*indentation
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = indent + indentation
        if not elem.tail or not elem.tail.strip():
            elem.tail = indent
        for elem in elem:
            fix_indentation(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = indent
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = indent
        
def remove_encoding_declaration(xml_string):
    return xml_string.replace('<?xml version="1.0" encoding="UTF-8"?>', '')

def prettify_gold(xml_doc: etree._Element, indentation="    "):
    """The built-in prettification doesn't work well for XML containing TEI annotations. We need to prettify manually"""
    # manually reindent the nodes
    fix_indentation(xml_doc, indentation=indentation)
    
    # wrap and indent formatted input
    for elem in xml_doc.xpath(r'//input[@type="formatted"]'):
        if type(elem.text) is str and elem.text != "": 
            prettify_content(elem)

    # fix the tail with incorrect whitespace    
    for elem in xml_doc.xpath(r'//*'):
        if type(elem.tail) is str and elem.tail != "": 
            fix_tail(elem)
    
    # stringify
    xml_string = etree.tostring(xml_doc, pretty_print=True).decode()
    
    # replace xml entities
    xml_string = html.unescape(xml_string)

    # remove superflous TEI namespace, this should be dealt with during tree creation
    xml_string = xml_string.replace('bibl xmlns="http://www.tei-c.org/ns/1.0"', 'bibl')
    xml_string = xml_string.replace('biblStruct xmlns="http://www.tei-c.org/ns/1.0"', 'biblStruct')
    xml_string = xml_string.replace('seg xmlns="http://www.tei-c.org/ns/1.0"', 'seg')

    return xml_string


def find_biblstruct(bibl_struct_tree, id):
     # find correspondent biblStruct data
    xpath_expr= f"//tei:biblStruct[substring(@source, string-length(@source) - {len(id) - 1})='{id}']"
    bibl_struct_matches = bibl_struct_tree.xpath(xpath_expr, namespaces=ns)
    if len(bibl_struct_matches) > 0:
        # found it - make a copy and remove unneeded attributes
        bibl_struct_copy = deepcopy(bibl_struct_matches[0])
        if 'source' in  bibl_struct_copy.attrib:
            del bibl_struct_copy.attrib['source']    
        # add it to target listBibl
        return bibl_struct_copy
    else:
        raise RuntimeError(f"Could not find matching biblStruct element with id '{id}'.")


def create_gold_standard(bibl_content, biblstruct_content, verbose=False, pretty=False):
    # get source trees and elements
    bibl_tree = etree.fromstring(remove_encoding_declaration(bibl_content))
    bibl_struct_tree = etree.fromstring(remove_encoding_declaration(biblstruct_content))

    # create output document
    root = etree.Element("dataset")

    # xpath expressions to find the parent node under which the <bibl> live
    bibl_parent_xpath_list = [ 
        '//tei:note[not(tei:p)] | //tei:note[tei:p]/tei:p',
        'listBibl' 
    ]
    
    process_log = []
    instance_id = 0
    
    for bibl_parent_xpath in bibl_parent_xpath_list:

        # iterate over elements, either listBibl, note, or p
        for parent_element in bibl_tree.xpath(bibl_parent_xpath, namespaces=ns):

            parent_tag = etree.QName(parent_element).localname
            parent_type = parent_element.attrib['type'] if 'type' in parent_element.attrib else None
            
            # new dataset instance
            instance_id += 1
            attrs = {}
            attrs[id_attrib] = f'instance-{str(instance_id)}'
            #attrs['source'] = 
            instance = etree.SubElement(root, 'instance', attrs)
            
            # input 
            child_xml = etree.tostring(parent_element, method="text", encoding='utf-8').decode()
            xml_content_text = remove_whitespace(child_xml)

            footnote_number = None
            if parent_type == "footnote":
                footnote_number = parent_element.attrib['n']
            if footnote_number:
                xml_content_text = f'{footnote_number} {xml_content_text}'
            
            input_raw = etree.SubElement(instance, 'input', {'type': 'raw'})
            input_raw.text = etree.CDATA(xml_content_text)
            input_formatted = etree.SubElement(instance, 'input', {'type': 'formatted'})
            input_formatted.text = xml_content_text

            # output
            output_bibl = etree.SubElement(instance, 'output', {'type': 'bibl'})
            output_bibl_p = etree.SubElement(output_bibl,'p', {"xmlns": tei_namespace})
            output_biblstruct = etree.SubElement(instance, 'output', {'type': 'biblStruct'})
            output_listBibl = etree.SubElement(output_biblstruct, 'listBibl', {"xmlns": tei_namespace})

            # iterate over all the contained elements, either bibl or seg
            for elem in parent_element:
                 
                elem_tag = etree.QName(elem).localname
                elem_type = elem.attrib['type'] if 'type' in elem.attrib else None

                if elem_tag == "bibl":
                    # the id of the bibl element in the document is required to look up the biblstruct
                    if id_attrib not in elem.attrib:
                        raise RuntimeError(f"Missing 'xml:id' attribute for bibl element: {elem}")
                    bibl_id = elem.attrib[id_attrib]
                    biblstruct = find_biblstruct(bibl_struct_tree, bibl_id)           
                    # add the biblStruct without xmlns
                    output_listBibl.append(biblstruct)
                    if 'xmlns' in biblstruct.attrib:
                        del biblstruct.attrib['xmlns']                        

                # add the element
                output_bibl_p.append(deepcopy(elem))

    # serialize to a pretty xml string with the linebreak issues addressed
    return prettify_gold(root)

def create_all_gold_standards(bibl_dir, biblstruct_dir, biblstruct_gold_dir, verbose=False):
    for file_path in glob(f'{bibl_dir}/*.xml'):
        file_id = os.path.basename(file_path).replace(".xml", "")

        bibl_path = file_path
        biblstruct_path = f'{biblstruct_dir}/{file_id}.biblstruct.xml'
        biblstruct_gs_path = f'{biblstruct_gold_dir}/{file_id}.xml'

        # log
        md_lines = [f'### Processing {file_id}']
        md_lines.append(f'Files: [TEI/bibl]({Path(bibl_path).as_posix()}) | [TEI/biblStruct]({Path(biblstruct_path).as_posix()}) | [Gold Standard]({Path(biblstruct_gs_path).as_posix()})')
        display(Markdown("\n".join(md_lines)))

        with (open(bibl_path, 'r', encoding='utf-8') as bibl_file,
              open(biblstruct_path, 'r', encoding='utf-8') as biblStruct_file):
            bibl_content = bibl_file.read()
            biblStruct_content = biblStruct_file.read()

        output_data = create_gold_standard(bibl_content, biblStruct_content, verbose=verbose, pretty=True)

        with open(biblstruct_gs_path, 'w', encoding='utf-8') as output_file:
            output_file.write(output_data)