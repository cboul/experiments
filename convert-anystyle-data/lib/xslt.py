import subprocess
import os
from glob import glob
import shutil

def transform(xslt_path, input_path, output_path, rename_extension:tuple=None):
    input_path = os.path.normpath(input_path)
    xslt_path = os.path.normpath(xslt_path)
    jar_path = os.path.normpath('./lib/SaxonHE12-5/saxon-he-12.5.jar')

    cmd = ['java', '-jar', jar_path,
           f'-s:{input_path}', f'-xsl:{xslt_path}', f'-o:{output_path}/',
           'p_target-language=de', 'p_github-action=true', f'p_output-folder={output_path}/']

    process = subprocess.run(cmd, capture_output=True, text=True)
    if process.returncode != 0:
        raise RuntimeError(process.stderr)

    # clean up the output dir
    for file_path in glob(f'{output_path}/*.xml'):
        os.remove(file_path)
    for file_path in glob(f'{output_path}/{output_path}/*.xml'):
        parent_dir_path = os.path.dirname(os.path.dirname(file_path))
        file_name = os.path.basename(file_path)
        shutil.move(file_path, os.path.join(parent_dir_path, file_name))
    shutil.rmtree(f'{output_path}/{output_path}', ignore_errors=True)

    # rename the extension
    if rename_extension:
        from_extension = rename_extension[0]
        to_extension = rename_extension[1]
        for file_path in glob(f'{output_path}/*.xml'):
            if file_path.endswith(from_extension):
                os.replace(file_path, file_path.replace(from_extension, to_extension))

    print(f'Applied {xslt_path} to files in {input_path} and saved result in {output_path}.')
    return process

