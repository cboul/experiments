import glob
import os
from difflib import HtmlDiff
from IPython.display import display, Markdown
import regex as re

def compare_input_strings(tei_dir_path, ref_dir_path, html_output_path, target_url, refs_output_path=None):
    os.makedirs(html_output_path, exist_ok=True)
    for tei_bibl_path in glob.glob(f'{tei_dir_path}/*.xml'):
        base_name = os.path.basename(tei_bibl_path)
        id = os.path.splitext(base_name)[0]
        anystyle_input_path = f'{ref_dir_path}/{id}.txt'

        # load original raw reference strings
        with open(anystyle_input_path, 'r', encoding='utf-8') as f:
            anystyle_input_data = f.read().splitlines()

        # load tei bibl to check
        with open(tei_bibl_path, 'r', encoding='utf-8') as r:
            tei_xml_doc = r.read()

        # convert it to raw reference text
        tei_input_data = tei_to_input(tei_xml_doc)

        # postprocess to fix issues that should be fixed in the source or in the converter code
        tei_input_data = fix_serialization_issues(tei_input_data)

        # if an output path has been given, store the reconstructed raw references
        if refs_output_path:
            with open(f'{refs_output_path}/{id}.txt', 'w', encoding='utf-8') as w:
                w.write("\n".join(tei_input_data))

        # create files showing the diff between the reconstructed and the original raw strings
        html_diff = HtmlDiff().make_file(fromlines=anystyle_input_data,
                                         tolines=tei_input_data,
                                         fromdesc=anystyle_input_path,
                                         todesc=tei_bibl_path)

        # store the visual diff
        with open(f"{html_output_path}/{id}.diff.html", "w", encoding="utf-8") as f:
            f.write(html_diff)
            display(Markdown(f'Extracted and compared input data for {id}  ([See diff]({target_url}/{id}.diff.html))'))

def fix_serialization_issues(text):
    # escape character sequences
    text = re.sub(r'\.\.\.', '[!elipsis!]', text)
    text = re.sub(r'\. \. \.', '[!spaced_elipsis!]', text)

    # remove duplicated punctuation
    text = re.sub(r'(\. ?){2}', r'.', text)

    # restore character sequences
    text = text.replace( '[!elipsis!]', '...')
    text = text.replace('[!spaced_elipsis!]', r'. . .')
    return text.strip()