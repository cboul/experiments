#!/usr/bin/env bash

bibutils_cmd=$1
ext=${bibutils_cmd#*2}
for file_path in mods/*.xml
do
  out_file=$(basename ${file_path/\.mods\.xml/.$ext})
  echo "Running $bibutils_cmd to convert $file_path to $ext/$out_file..."
  $bibutils_cmd -i utf8 -o utf8 "$file_path" > "$ext/$out_file" 2>&1
done