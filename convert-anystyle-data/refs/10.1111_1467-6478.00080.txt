1 For a contrary view, see G. Jones, ‘ “Traditional” Legal Scholarship: a Personal View’ in What Are Law Schools For?, ed. P. Birks (1996) 14.
3 R. Goff, ‘The Search for Principle’ (1983) Proceeedings of the British Academy 169, at 171. This is an amplification of Dicey’s remark that ‘[b]y adequate study and careful thought whole departments of law can . . . be reduced to order and exhibited under the form of a few principles which sum up the effect of a hundred cases . . .’. A. Dicey, Can English Law be taught at the Universities? (1883) 20.
4 J. Smith, The Law of Contract (1989)
6 See, for example, D. Kennedy, ‘Form and substance in Private Law Ajudication’ (1976) 89 Harvard Law Rev. 1685.
7 B. Hepple, ‘The Renewal of the Liberal Law Degree’ (1996) Cambridge Law J. 470, at 485 and 481.
8 P.A. Thomas, ‘Introduction’ in Socio-Legal Studies, ed. P.A. Thomas (1997) 19.
9 R. Cotterrell, Law’s Community (1995) 296.
10 Socio-legal studies has been defined in many different ways. In this essay the term is taken to indicate the use of ideas ‘from other disciplines primarily but not exclusively from within the social science and humanities fields’. S. Wheeler, ‘Company Law’ in Thomas, op. cit., n. 8, at p. 285.
11 Some fail wholly. It is difficult to see any effect on academic legal education that resulted from Lady Marre’s report A Time for Change (1988). The Jarratt report on universities produced for the Committee of Vice-Chancellors and Principals (CVCP), Report of the Steering Committee for Efficiency studies in Universities (1988), produced much comment but little action. Even those that are thought of as being a success are not wholly implemented. Despite Ormrod’s recommendations no Institute of Profesional Legal Studies was set up and the universities and colleges of higher education did not take sole responsibility for vocational legal training (Report of the Committee on Legal Education (1971; Cmnd 4595) ch. 9, recs. 40 and 23). There were also other recommendations that were not implemented. The Robbins report on higher education, Higher Education (1963; Cmnd. 2154) took it is axiomatic that ‘courses of higher education should be available for all those who are qualified by ability and attainment to pursue them and wish to do so’ (para. 31). This has yet to happen.
12 National Committee of Inquiry into Higher Education, Higher Education in the learning society (1997) (the Dearing report); ACLEC, First Report on Legal Education and Training (1996). The Government’s White Paper on further and higher education had not been published at the time of writing this essay. 13 ACLEC, id., para 4.6.
14 ACLEC’s proposal is part of an historical process which has gradually seen English university law schools distance themselves from the legal professions and the legal professions propose decreasing degrees of control over the content of law degrees. (See A. Bradney and F. Cownie, ‘Working on the Chain Gang?’ (1996) 2 Contemporary Issues in Law 15, at 24–6).
15 J. MacFarlane, M. Jeeves, and A. Boon, ‘Education for Life or for Work?’ (1987) 137 New Law J. 835, at 836.
16 T.H. Huxley, ‘Universities: Actual and Ideal’ in T.H. Huxley, Collected Essays: Volume III (1905) 215.
17 J.S. Mill, ‘Inaugural address to the University of St Andrews’ in Collected Work of John Stuart Mill: Volume XXI, ed. J.M. Robson (1984) 218.
18 Dearing, op. cit., n. 12, para. 9.32.
19 id., para. 5.11.
20 F.R. Leavis, Education and the University (1948) 28. Leavis’s view was narrowly nationalistic. For ‘centre’ it would be better to substitute ‘centres’.
21 See, further, A. Bradney, ‘Liberalising Legal Education’ in The Law School: Global Issues, Local Questions, ed. F. Cownie (forthcoming).
22 P. Goodrich, ‘ Of Blackstone’s Tower: Metephors of Distance and Histories of the English Law School’ in Birks, op. cit., n. 1, p. 59.
23 S. Turow, One L (1977) 106.
24 O. Kahn-Freund, ‘Reflections on Legal Education’ (1966) 29 Modern Law Rev. 121, at 129.
25 Kahn-Freund believed ... legal argument (Kahn-Freund, id.).
26 Leavis, op. cit., n. 20, p. 120.
29 Leavis has, of course, been widely criticized for the cultural and gender assumptions that lie behind his selection of material to be studied. (See, for example, M. King, The New English Literatures (1980) at 216–17.) Whatever the accuracy of these criticisms, they are criticisms of the application of the method rather than the method itself.
30 Jurisprudence by Sir John Salmond, ed. G. Willliams (10th ed., 1947) at 256 and 257.
31 So much so that when other disciplines engage with law they must develop their own concepts to analyse law rather than rely on the concepts already developed in law. See, for example, E. Durkheim The Division of Labour in Society (1933) 68.
32 M. Le Brun and R. Johnstone, The Quiet Revolution: Improving Student Learning in Law (1994) 65. On the effect on women students, see ‘Define and Empower: Women Students Consider Feminist Learning’ (1990) I Law and Critique 47 at pp. 54–55. For a survey of CLS and feminist literature on this general point, see W. Conklin, ‘The Invisible Author of Legal Authority’ (1996) VII Law and Critique 173 at pp. 173–6.
33 R. Collier, ‘Masculinism, Law and Law Teaching’ (1991) 19 International J. of the Sociology of Law 427, at 429.
34 P. McAuslan, ‘Administrative Law, Collective Consumption and Judicial Policy’ (1983) 46 Modern Law Rev. 1, at 8.
35 Le Brun and Johnstone, op. cit, n. 32, pp. 71–5.
38 Goodrich, op. cit., n. 22.
39 P. Samuelson, ‘The Convergence of the Law School and the University’ (1975) 44 The Am. Scholar 256, at 258.
40 P. Harris and M. Jones ‘A Survey of Law Schools in the United Kingdom, 1996’ (1997) 31 The Law Teacher 38, at 46.
41 J. Wilson , ‘A third survey of university legal education’ (1993) 13 Legal Studies 143, at 152.
42 Thus, for example, Harris and Jones reported that 59.2 per cent of all particpating institutions offered foriegn language tuition as part of their standard LLB programme. (Harris and Jones, op. cit., n. 40, at p. 54).
43 P. Leighton, T. Mortimer, and N. Whatley, Law Teachers: Lawyers or Academics? (1995) 34. This would include teaching both non-law degree students and sub-degree students.
44 id., p 35
45 L. Skwarok, ‘Business Law for Non-Lawyers: Setting the Stage for Teaching, Learning and Assessment at Hong Kong Polytechnic University’ (1995) 29 The Law Teacher 189, at 189.
46 N. Bastin, ‘Law, Law Staff and CNAA Business Studies Degree Courses’ (1985) 19 The Law Teacher 12, at 13.
47 A. Ridley, ‘Legal Skills for Non-Law Students: Added Value or Irrelevant Diversion?’ (1994) 28 The Law Teacher 281, at 282.
48 G. Cartan and T. Vilkinas, ‘Legal Literacy for Managers: The Role of the Educator’ (1990) 24 The Law Teacher 246, at 248.
49 Ridley, op. cit., n. 47, at p. 284.
50 This, of course, is not always the case. For example, the BA Economics and Law degree at Leicester has a special course in each year given over to the consideration of the relationship between economics and law.
51 P. Birks, ‘Short Cuts’ in Pressing Problems in the Law, ed. P. Birks (1994) 10–24.
52 Ridley, op. cit., n. 47, p. 283.
53 Cartan and Vilkinas, op. cit., n. 48, p. 248.
54 P. Harris, ‘Curriculum Development in Legal Studies’ (1986) 20 The Law Teacher 110, at 112.
55 Dearing, op. cit., n. 12, para 9.3.
57 G. Steiner, Errata: An Examined Life (1997) 20.