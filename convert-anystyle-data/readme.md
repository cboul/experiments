# Conversion of AnyStyle training data to other formats

This subrepo contains code to convert the existing training data in the AnyStyle formats (XML, TTX) into other formats.

Note: the requirements introduce a huge dependency tree - use a virtual environment to avoid cluttering your python installation.
